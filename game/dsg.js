var dsg = (function () {
	console.log("Starting DSG");
	
	this.pathSep = '_';
	
	var textArea = document.getElementById('text');
	
	this.loaded = {};
	
	// Show image
	this.showImg = function( path ){
		main.style.backgroundImage = "url('" + path + "')";
	}
	
	this.setEmptyBg = function( color ){
		main.style.backgroundImage = null;
		main.style.backgroundColor = color;
	}
	
	this.emptyLoc = function(){
		while(textArea.firstChild){
			textArea.removeChild(textArea.firstChild);
		}
	}
	
	/**
	* Load an object
	* id: object identifier
	* original: don t load saved game specific changes
	*/
	this.load = function( id, original ){
		
		return new Promise( function(resolve, reject) {
			
			if( loaded[id] ){
				resolve( loaded[id] );
			}
			var objStr = original ? null : localStorage.getItem( id );
			if( objStr ){
				var obj = JSON.parse( objStr );
				if( obj.parentId ){
					this.load( parentId ).then( function( obj ){
						obj.prototype = parentObj;
						loaded[id] = obj;
						if( obj.onLoad ){
							obj.onLoad();
						}
						resolve( obj );
					}, function(){
						console.log('Error loading parent ' + parentId);
						reject();
					});
				}
			}else{
				var path = 'game/' + id.replace(pathSep,'/') + '.js';
				
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.open("GET", path);
				xmlhttp.onreadystatechange = function(){
					if( xmlhttp.readyState == 4 ){
						if ( xmlhttp.status == 200 ){
							try{
								eval(xmlhttp.responseText);
								resolve( loaded[id] );
							}catch( err ){
								console.log( "Evaluation error loading " + id );
								console.log( err );
								reject(Error("Evaluation error loading " + id));
							}
						}else{
							reject(Error("Network Error loading " + id));
						}
					}
				};
				xmlhttp.send();
			}

		});
	}
	
	this.save = function ( id, obj ){
		return new Promise( function(resolve, reject) {
			
			this.load(id, true).then( function( original ){
				// diff per property, if equal => don't save
			}, function(){
				reject(Error("Error getting original of " + id));
			} );
		});
	};
	
	// Clear choices
	this.clearChoices = function(){
		var ch = textArea.getElementsByClassName("A");
		while(ch[0]) {
			ch[0].parentNode.removeChild(ch[0]);
		}
	};
	
	// Add choice
	this.choice = function(label, onClick, cssclass){
		var choice = document.createElement("A");
		choice.setAttribute("class", cssclass ? cssclass : "choice");
		var textnode = document.createTextNode( label );
		choice.appendChild(textnode);
		choice.onclick = onClick;
		textArea.appendChild(choice);
	};
	
	this.tag = function(tag, txt){
		var e = document.createElement(tag);
		var textnode = document.createTextNode( txt );
		e.appendChild(textnode);
		textArea.appendChild(e);
	};
	
	this.h1 = function(txt){
		this.tag('H1', txt);
	}
	
	this.p = function(txt){
		this.tag('P', txt);
	}
	
	this.enter = function( locId ){
		this.load( locId ).then( function( loc ){
			if(loc.onEnter)
				loc.onEnter();
		} );
	};
	
	// Init loop
	this.init = function(){
		if( !localStorage ){
			alert('Your browser is too old tu run this game, please download a brand new one');
			return;
		}
		if( !localStorage.getItem('acceptedWarning') ){
			this.enter( 'loc' + pathSep + 'warning' );
		}else{
			var currentLoc = localStorage.getItem('currentLocation');
			if( !currentLoc ){
				this.load( 'loc' + pathSep + 'start' );
			}else{
				this.load( currentLoc );
			}
		}
	};
	
	return this;
})();

dsg.init();

