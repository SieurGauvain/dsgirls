dsg.loaded.loc_warning = (function(){
	this.parentId = 'location';
	
	this.onEnter = function(){
		console.log('warning load');
		dsg.emptyLoc();
		dsg.setEmptyBg('white');
		
		dsg.h1('Avertissement!');
		
		dsg.p('La présente oeuvre est destinée à un publique adulte, amateur de grivoiserie et de second degré. Il contient ' +
		'des dialogues et des images crues qui ne sont pas adaptées à un publique prude et sensible.');
		
		dsg.p('L\'ensemble des personnes, des oeuvres, des marques et des institutions citées dans cette oeuvre font partie d\'une histoire complètement fictive dont le but unique est de faire rire. S\'ils devaient exister dans la réalité, ce ne serait le fruit d\'un pur hasard ou d\'une certaine ironie et rentrent dans le cadre L 122-5 du Code de la Propriété Intellectuelle.');

		dsg.p('Cette oeuvre prétend pas promouvoir aucune des pratiques citées et nous attirons le lecteur sur la différence entre le fantasme et la réalité. Si vous tentez quoi que ce soit y resemblant à la maison, vous être prévenus !');
		
		dsg.choice('J\'accepte', function(){ dsg.enter( 'loc' + dsg.pathSep + 'start' ); }, "choice main");
		dsg.choice('Je refuse', function(){window.location.href = 'http://www.google.com'});
	};
	
	return this;
})();
//# sourceURL=loc/warning.js