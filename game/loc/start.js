dsg.loaded.loc_start = (function () {
	this.parentId = 'location';
	
	this.onEnter = function(){
		console.log('start load');
		dsg.emptyLoc();
		dsg.showImg('game/loc/warning/ds-paris.jpg');
		
		dsg.h1('Il était une fois...');
		
		dsg.p('La présente oeuvre est destinée à un publique adulte, amateur de grivoiserie et de second degré. Il contient ' +
		'des dialogues et des images crues qui ne sont pas adaptées à un publique prude et sensible.');
		
		dsg.choice('Suivant', function(){ this.enter( 'loc' + dsg.pathSep + 'warning' ); });
	};
	
	return this;
})();
//# sourceURL=loc/start.js